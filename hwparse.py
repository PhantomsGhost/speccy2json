
import re
from config import *

"""
Produces the hardware checks for various things from a speccy report
"""



### Check for a Killer Network Interface
### Their software suite and drivers
### are known to cause issues
def getKillerNet(body):
    try:
        if "killer" in body.find("div", text=re.compile("Device Tree")).next_sibling.next_sibling.next_sibling.next_sibling.text.lower():
            return("True")
        else:
            return("False")
    except:
        if "killer" in body.find("img",src="img/256.png").next_element.next_element.next_element.text.lower():
            return("True")
        else:
            return("False")
    else:
        return("Unknown")

### Generate a summary of the hardware in the system
def getHardwareSum(body):
    summary = {
        "CPU" :"",
        "Motherboard":"",
        "RAM" :"",
        "GPUs":[],
        "Storage":[],
        "TempChecks":[]
    }
    for hwitem in ["CPU","Motherboard","RAM"]:
        try:
            hwtemp = body.find("div", class_="blue clear", text=hwitem).next_sibling.next_sibling.next_element.next_element.find(class_="datavalue").text.split(" °")[0]
            if hwtemp:
                if int(hwtemp) > TEMP_LIMIT:
                    summary["TempChecks"].append(f"{hwitem} - {hwtemp} °C" )
        except:
            pass
        try:
            component = re.sub("[^\x00-\x7F]+","",body.find("div", class_="blue clear", text=hwitem).next_sibling.next_sibling.next_element.next_element.find(class_="datakey").text.replace(":\xa0","").strip())
            if component:
                summary[hwitem] = component
            else:
                summary[hwitem] = "Not Listed"
        except:
            summary[hwitem] = "Unknown"
    try:
        summary["CPU"] = body.find_all("div", class_="datakey", text=re.compile("Specification:"))[0].next_sibling.next_sibling.text
    except:
        summary["CPU"] = "Unknown"
    try:
        for graph in body.find("div", class_="blue clear", text="Graphics").next_sibling.next_sibling.find_all(class_ = "datakey"):
            ### This needs cleaned up at somepoint
            if "nvidia" in graph.text.lower() or \
                "amd"    in graph.text.lower() or \
                "ati"    in graph.text.lower() or \
                "intel"  in graph.text.lower() or \
                "geforce"  in graph.text.lower() or \
                "basic display" in graph.text.lower() or \
                "remote display" in graph.text.lower() or \
                "graphics adapter" in graph.text.lower():
                summary["GPUs"].append(graph.text.replace(":\xa0",""))
                try:
                    hwtemp = graph.next_sibling.next_sibling.text.split(" °")[0]
                    if hwtemp:
                        if int(hwtemp) > TEMP_LIMIT:
                            GPUID = graph.text.replace(':\xa0','')
                            summary["TempChecks"].append(f"{GPUID} - {hwtemp} °C")
                except:
                    pass
        if not summary["GPUs"]:
            summary["GPUs"].append("None Listed")
    except:
        summary["GPUs"].append("Unknown")
        
    try:
        for storage in body.find("div", class_="blue clear", text="Storage").next_sibling.next_sibling.find_all(class_ = "datakey"):
            summary["Storage"].append(storage.text.replace(":\xa0",""))
            try:
                    hwtemp = storage.next_sibling.next_sibling.text.split(" °")[0]
                    if hwtemp:
                        if int(hwtemp) > TEMP_LIMIT:
                            storageID = storage.text.replace(':\xa0','')
                            summary["TempChecks"].append(f"{storageID} - {hwtemp} °C" )
            except:
                pass
    except:
        summary["Storage"].append("Unknown")            
    return(summary)


### Check for mismatched memory based on Part Numbers
### Ignores systems with single sticks of memory
def getMemMismatch(body):
    try:
        if not len(body.find("div", class_="blue clear", text="SPD").next_sibling.next_sibling.next_sibling.next_sibling.find_all(text=re.compile("Slot #"))):
            return("Unknown")
        elif len(body.find("div", class_="blue clear", text="SPD").next_sibling.next_sibling.next_sibling.next_sibling.find_all(text=re.compile("Slot #"))) == 1: 
            return("No Mismatch")
    except:
        return("Unable to detect memory")
    try:
        memList = []
        for slot in body.find("div", class_="blue clear", text="SPD").next_sibling.next_sibling.next_sibling.next_sibling.find_all(text=re.compile("Slot #")):#.next_element.next_element:
            pn = slot.next_element.next_element.find(text=re.compile("Part Number:")).next_element.next_element.text
            if len(memList) == 0:
                memList.append(pn)
            elif pn not in memList:
                return("Mismatched")
        return("No Mismatch")
    except:
        return("Unknown")
    
### Checks for a potential XMP Overclock using
### the maximum bandwidth of the memory and
### the current speed of the memory
def checkXMP(body):
    try:
        jedecSpeed = float(body.find("div", class_="blue clear", text="SPD").next_sibling.next_sibling.next_sibling.next_sibling.find(text=re.compile("Slot #1")).next_element.next_element.find(text=re.compile("Max Bandwidth:")).next_element.next_element.text.split("(")[1].split(" MHz)")[0])
        currentFreq = float(body.find("div", class_="datakey", text=re.compile("DRAM Frequency:")).next_sibling.next_sibling.text.split(" MHz")[0])
        if abs(currentFreq-jedecSpeed) > 100:
            return(f"Possible XMP Detected - {int((abs(currentFreq-jedecSpeed)*2))} MHz delta")
        else:
            return("Likely at stock")
    except:
        return("Unable to Check")