import re
import time

"""
Produces the drive data for the speccy summary
"""

### Reads the SMART data from all drives in a system 
### and translates it into a JSON format
def genSmartJson(body):
    try:
        drives = {}
        partitionValues = ["Partition ID","Disk Letter","File System","Volume Serial Number","Size","Used Space","Free Space"]
        drives_found = body.find("div", class_="blue clear", text="Hard drives").next_sibling.next_sibling.next_sibling.next_sibling.find_all("div", class_="blue clear", text="S.M.A.R.T")

        for drive in drives_found:
            driveName = drive.previous_element.parent.previous_sibling.previous_sibling.previous_element.previous_element
            driveInfo = drive.previous_element.parent.previous_sibling.previous_sibling
            if "Serial Number" in driveInfo.text:
                serialNumber = driveInfo.find("div", class_="datakey", text=re.compile("Serial Number")).next_sibling.next_sibling.text
            else:
                serialNumber = time.time()
            drives[f"{driveName}-{serialNumber}"]={}

            driveData = drive.previous_element.parent
            partData = {}

            for partition in driveData.find_all("div", class_="blue clear", text=re.compile("Partition")):
                partData[partition.text] = {}
                for pv in partitionValues:
                    try:
                        partData[partition.text][pv] = partition.next_sibling.next_sibling.find("div", class_="datakey", text=re.compile(pv)).next_sibling.next_sibling.text
                    except:
                        partData[partition.text][pv] = "N/A"

            smartData = {}
            if "S.M.A.R.T not supported" != driveData.find("div", class_="blue clear", text=re.compile("S.M.A.R.T")).next_sibling.next_sibling.text.strip():           
                drives[f"{driveName}-{serialNumber}"]["SmartStatus"] = driveData.find("div", class_="blue clear", text=re.compile("S.M.A.R.T")).next_sibling.next_sibling.find("div", class_="datakey", text=re.compile("Status")).next_sibling.next_sibling.text
                smart = driveData.find("div", class_="blue clear", text=re.compile("attributes")).next_sibling.next_sibling.next_sibling.next_sibling
                for sd in smart.find_all("div", class_="blue clear"):
                    smartData[sd.text] = {}
                    smartData[sd.text]["AttributeName"] = sd.next_sibling.next_sibling.find("div", class_="datakey", text=re.compile("Attribute")).next_sibling.next_sibling.text
                    smartData[sd.text]["RawValue"] = sd.next_sibling.next_sibling.find("div", class_="datakey", text=re.compile("Raw Value")).next_sibling.next_sibling.text
            else:
                drives[f"{driveName}-{serialNumber}"]["SmartStatus"] = "Not Supported"

            driveKeys = ["Interface","Capacity","RAID"]
            for key in driveKeys:
                drives[f"{driveName}-{serialNumber}"][key] = driveInfo.find("div", class_="datakey", text=re.compile(key)).next_sibling.next_sibling.text
            drives[f"{driveName}-{serialNumber}"]["SMART Attributes"] = smartData
            drives[f"{driveName}-{serialNumber}"]["Partitions"] = partData
    except:
        drives = {}
    return(drives)

### Uses drive json data and checks the smart data to estimate drive health
### Good: SMART Overall != Bad, No CRC or Sector issues in SMART
### Warning: SMART Overall != Bad, CRC errors, but no detected Sector Issues
### Bad: SMART Overall is Bad or there are detected Sector issues in SMART
def checkDrive(drive):
    driveInfo = drive
    majorAttr = ["Reallocated Sectors Count","Reported Uncorrectable Errors","Current Pending Sector Count","Uncorrectable Sector Count","Uncorrectable Error Count","Reallocation Event Count"]
    minorAttr = ["UltraDMA CRC Error Count","CRC Error Count","SATA R-Errors (CRC) Error Count"]

    ### Generate attribs:
    warn_attribs = []
    bad_attribs = []
    if driveInfo['SMART Attributes']:
        if driveInfo["SmartStatus"] == "Good" or driveInfo["SmartStatus"] == "Unknown" or driveInfo["SmartStatus"] == "Warning":
            attribs  = {}
            for attr in driveInfo['SMART Attributes']:
                attribs[driveInfo['SMART Attributes'][attr]["AttributeName"]] = driveInfo['SMART Attributes'][attr]["RawValue"]
            for major in majorAttr:
                if major in attribs.keys():
                    if attribs[major] != "0000000000":
                        bad_attribs.append(f"{major[:15]} = {attribs[major].strip('0')}")
                        #return("Bad")
            for minor in minorAttr:
                if minor in attribs.keys():
                    if attribs[minor] != "0000000000":
                        warn_attribs.append(f"{minor[:15]} = {attribs[minor].strip('0')}")
                        #return("Warning")
            if not warn_attribs and not bad_attribs:
                return("Good")
            elif bad_attribs:
                return("Bad\n" + "\n\n".join(bad_attribs)+"\n\n")
            elif warn_attribs:
                return("Warning\n" + "\n\n".join(warn_attribs)+"\n\n")
            else:
                return("Code 37")
        else:
            return(driveInfo["SmartStatus"])
    else:
        return(driveInfo["SmartStatus"])

### Attempts to get the drive health of all drives in a system    
def getDriveHealth(drives):
    # Get Drives SMART Status
    # Modified from Jessexd
    badDrives = []
    for drive in drives:
        status = checkDrive(drives[drive])
        print(status)
        if status != "Good" and "Not Supported" not in status:
            badDrives.append(f"{drive} is {status}")
    return("|".join(badDrives))

### Attempts to get the free space of every lettered partition
### in a system.
def getFreeSpace(drives):
    lowParts = []
    for drive in drives:
        for partition in drives[drive]["Partitions"]:
            ptdata = drives[drive]["Partitions"][partition]
            if ptdata["Free Space"] != "N/A" and ptdata["Disk Letter"] != "N/A":
                free = ptdata["Free Space"].replace(" ","").split("(")[1].replace("%)","")
                if int(free) <=15:
                    lowParts.append(f"{ptdata['Volume Serial Number']} / {ptdata['Disk Letter']} has {free}% of {ptdata['Size']} remaining.")
    return("|".join(lowParts))