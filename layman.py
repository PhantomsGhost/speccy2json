# Create an english explanation of detected issues
import datetime


def layman(data):
    layExplain = ""
    supportIssue = ""

    OS = data["OSCheck"]["MajorOS"]+ data["OSCheck"]["MinorOS"]
    if data["OSCheck"]["OSSupported"] == "Not Supported":
        supportIssue += f"Your OS is currently End-Of-Life. (?eol) {OS}\n"
    elif data["OSCheck"]["OSSupported"] =="Unsupported Edition":
        supportIssue += f"You are running an edition of Windows we do not support! {OS}\n"
    elif data["OSCheck"]["OSSupported"] == "Unknown Support":
        layExplain += f"Your OS Support cannot be detected. Please send a screenshot of `winver`\n"

    if data["OSCheck"]["MinorOS"] == "2004":
        eol2004 = datetime.datetime(2021, 12, 14)
        delta = eol2004 - datetime.datetime.now()
        supportIssue += f"Your OS will be End Of Life in less than {delta.days} days!\n"

    # Windows 11 was released early. With the large amount of users on 
    # Insider builds potentially. We will require a Winver check
    if "11" in data["OSCheck"]["MajorOS"]:
        supportIssue += f"In order to verify you are on the official release, please send a screenshot of `Winver`\n"

    FU = data["OSCheck"]["FailedHotfix"]
    if data["OSCheck"]["FailedHotfix"] >= 20:
        layExplain += f"You have a decent number of failed updates {FU}\n"
    elif data["OSCheck"]["FailedHotfix"] >= 75:
        layExplain += f"You have a large number of failed updates {FU}\n"

    if data["OSCheck"]["BPPC"] == "True":
        supportIssue += "We cannot provide you support due to pirated software detected. \n"*3

    if data["SecurityCheck"]["AVStatus"] != "Windows Defender":
        layExplain += "Please only use Windows Defender (?av)\n"

    if data["SecurityCheck"]["WindowsDefenderStatus"] != "Enabled":
        layExplain += "Please keep Windows Defender enabled.\n"

    if data["SecurityCheck"]["FirewallState"] != "Enabled":
        layExplain += "Please keep your firewall enabled.\n"
    
    if "0" in data["SecurityCheck"]["UACLevel"] or "1" in data["SecurityCheck"]["UACLevel"]:
        layExplain += "It is recommended to have a UAC Level of atleast 2.\n"
    
    if data["SoftwareCheck"]["NoNoList"] != "False":
        NL = data["SoftwareCheck"]["NoNoList"]
        layExplain += f"The following programs/software have been known to cause issues: {NL}\n".replace("False","🐟")

    if data["SoftwareCheck"]["AVDetect"] != "False":
        AV = data["SoftwareCheck"]["AVDetect"]
        layExplain += f"Adding additional or third party AV/AM software is not recommended on personal systems: {AV} (?av)\n"

    if "Up" not in  data["SoftwareCheck"]["SpeccyVersion"] :
        supportIssue += "Please use an up to date version of Speccy to continue support!\n"

    if "Bad" in data["HardwareCheck"]["BadDrives"]:
        layExplain += "You have at least 1 failing drive in your system.\n"
    
    if "Warning" in data["HardwareCheck"]["BadDrives"]:
        layExplain += "You have at least 1 drive with CRC Errors in your system.\n"

    if data["HardwareCheck"]["FreeSpaceIssues"]:
        layExplain += "You have at least 1 drive low on space.\n"

    if data["HardwareCheck"]["MixedMemory"] == "Mismatched":
        layExplain += "Please avoid mixing memory. This can lead to issues.\n"

    if data["HardwareSummary"]["TempChecks"]:
        layExplain += "You have some toasty hardware. Unless requested, please run speccy at idle.\n"
    
    notes = "\n"
    if supportIssue:
        notes += """\n**!!!**\n""" + supportIssue +"""**!!!**\n\n"""
    if layExplain:
        notes += layExplain
    return(notes)
