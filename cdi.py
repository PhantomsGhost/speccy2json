import pandas as pd
import numpy as np
from datetime import datetime
import os
import time, random,re
from hwconfig import headings
from urllib.parse import quote
import json




def isValidURL(url):
    """Makes sure that the URL is valid and expected

    Args:
        url (str): URL of the CDI report from discord.

    Returns:
        Bool: True/False if the URL is valid.
    """
    # https://www.geeksforgeeks.org/check-if-an-url-is-valid-or-not-using-regular-expression/
    # Regex to check valid URL
    regex = ("((http|https)://)(www.)?" +
             "[a-zA-Z0-9@:%._\\+~#?&//=]" +
             "{2,256}\\.[a-z]" +
             "{2,6}\\b([-a-zA-Z0-9@:%" +
             "._\\+~#?&//=]*)")
     
    # Compile the ReGex
    p = re.compile(regex)
 
    # If the string is empty
    # return false
    if (url == None):
        return False
 
    # Return if the string
    # matched the ReGex
    
    if(re.search(p, url)) and url[len(url)-3:len(url)].lower() == "txt"\
        and "https://cdn.discordapp.com/attachments/" in url and " " not in url and ";" not in url:
        return True
    else:
        return False


def parseinfo(url):
    """Takes a URL and parses the information from the CDI report

    Args:
        url (str): Input URL of the CDI report.
    """
    if isValidURL(url) == False:
        return("Invalid")
    new_url = quote(url).replace("https%3A//","http://")
    print(new_url)
    fname = f"/tmp/{time.time()}{random.randint(1,100)}.cdi"
    os.system(f"wget {new_url} -O {fname}")
    
    def clearsplit(inp):
        """Removes line classifier from the input string

        Args:
            inp (str): Line from the CDI report.
        """

        return(inp.replace("-.-",""))


    def driveheader(inp):
        """Generates a drive header to split on 

        Args:
            inp (str): Name of the drive
        """        
        
        return(f"""-.-----------------------------------------------------------------------------\n-.-{inp}\n-.-----------------------------------------------------------------------------\n""")

    major_attr = ["Reallocated Sectors Count","Reported Uncorrectable Errors","Current Pending Sector Count","Uncorrectable Sector Count","Uncorrectable Error Count","Reallocation Event Count"]
    minor_attr = ["UltraDMA CRC Error Count","CRC Error Count","SATA R-Errors (CRC) Error Count"]

    # Load data
    with open(fname) as f:
        lines = f.readlines()
        data = "-.-".join(lines).split("-.--- IDENTIFY_DEVICE ---------------------------------------------------------")

    def getDrives():
        """Get all of the drives from the CDI report"""        

        for drive in (clearsplit(data[0].split("-.-----------------------------------------------------------------------------")[1].split("-.--- Disk List ---------------------------------------------------------------")[1])).split("\n"):
            if drive.split(" : ")[0]:
                drives[drive.split(" : ")[0]] = {}

    def getSMART():
        """Get all of the SMART attributes from the current drive"""
        smart_data = {}
        for _ in drivedata.split("\n"):
            if "---------------------------------------------------" in _:
                splitter = _
        ds = drivedata.split(splitter)[1]
        header = ds.split("\n")[1]
        if header == "ID RawValues(6) Attribute Name": #[3:15]
            for attr in ds.split("\n")[2:]:
                if len(attr) > 10:
                    smart_data[attr[16:]] = attr[3:15]
        else:
            for attr in ds.split("\n")[2:]:
                if len(attr) > 10:
                    smart_data[attr[28:]] = attr[15:27]
        return(smart_data)
        
    drives = {}
    getDrives()
            
    _index = 0

    # Get the SMART data for each drive
    for drive in drives.keys():
        try:
            drivedata = (clearsplit(data[_index].split(driveheader(drive))[1]))
            for descriptor in drivedata.split("\n"):
                if ":" in descriptor:
                    drives[drive][descriptor.split(" : ")[0].strip()] = descriptor.split(" : ")[1].strip()
            drives[drive]["SMART"] = getSMART()
        except Exception as e:
            print(e)
            pass
        _index += 1

    def get_health(drive_smart,attributes,log_msg):
        """Get the health of the drive based on smart attributes and return log_msg if the drive is not healthy
        Now with extra returns because Ajax

        Args:
            drive_smart (dict): Dictionary of the drive's smart attributes and values
            attributes (list): List of attributes to check against
            log_msg (str): Message to return if there are flagged attributes

        Returns:
            str: log_msg or cleared
            flag_attributes (int): Number of flagged attributes
            len(drive_smart.keys()) (int): Number of attributes total
        """         

        flag_attributes = 0

        for attr in attributes:
            if attr in drive_smart.keys():
                print(attr,drive_smart[attr])
                if drive_smart[attr] != "000000000000":
                    flag_attributes += 1
        if flag_attributes:
            return(log_msg,flag_attributes,len(drive_smart.keys()))
        else:
            return("",flag_attributes,len(drive_smart.keys()))
    #What
    simple_drives = {}
    def drives_status():
        """Generate JSON for all of the drives and prepare for return"""        
        for drive in drives.keys():
            simple_drives[drive] = {}
            simple_drives[drive]["Model"] = drives[drive]["Model"]
            simple_drives[drive]["RPM"] = drives[drive]["Rotation Rate"]
            simple_drives[drive]["Power On Years"] = round(int(drives[drive]["Power On Hours"].split(" ")[0]) / (24*365),2)
            simple_drives[drive]["Power On Count"] = drives[drive]["Power On Count"].split(" ")[0]
            simple_drives[drive]["Drive Letter(s)"] = drives[drive]["Drive Letter"]
            simple_drives[drive]["Interface"] = drives[drive]["Interface"]
            simple_drives[drive]["CDI Health"] = drives[drive]["Health Status"]
            bad_status,bad_flag,total_attr = get_health(drives[drive]["SMART"],major_attr,"BAD")
            warn_status,warn_flag,total_attr = get_health(drives[drive]["SMART"],minor_attr,"Warning")
            total_flag = bad_flag + warn_flag
            check_msg = f" | {total_flag}/{total_attr} Attr Flagged"
            print(bad_status,warn_status)
            if bad_status and warn_status:
                simple_drives[drive]["r/TS Health"] = f"Bad drive with CRC Errors!" + check_msg
            elif bad_status:
                simple_drives[drive]["r/TS Health"] = f"Bad drive!" + check_msg
            elif warn_status:
                simple_drives[drive]["r/TS Health"] = f"CRC Errors detected." + check_msg
            else:
                simple_drives[drive]["r/TS Health"] = f"Smart Data Cleared" + check_msg
 

    drives_status()
    os.system(f"rm -f {fname}")
    return(json.dumps(simple_drives))
