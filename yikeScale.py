import json

"""
Produces the "Yikes" rating for speccy reports
"""

def YSR(data):
    ysr = 0

    # OS Checks
    if data["OSCheck"]["OSSupported"] != "Supported":
        ysr += 1
    if data["OSCheck"]["MinorOS"] == "2004":
        ysr += 2
    if data["OSCheck"]["FailedHotfix"] > 100:
        ysr += int(int(data["OSCheck"]["FailedHotfix"]) * 0.031415926535)
    elif data["OSCheck"]["FailedHotfix"] > 100:
        ysr += 1.5 
    elif data["OSCheck"]["FailedHotfix"] > 50:
        ysr += 1
    elif data["OSCheck"]["FailedHotfix"] > 25:
        ysr += 0.5
    elif data["OSCheck"]["FailedHotfix"] > 10:
        ysr += 0.25

    if data["OSCheck"]["BPPC"] == "True":
        ysr += 1

    #Security
    if data["SecurityCheck"]["AVStatus"] == "Disabled":
        ysr += 2
    elif "|" in data["SecurityCheck"]["AVStatus"]:
        ysr += data["SecurityCheck"]["AVStatus"].count("|") * 0.5


    if data["SecurityCheck"]["WindowsDefenderStatus"] != "Enabled":
        ysr += 1

    if data["SecurityCheck"]["FirewallState"] != "Enabled":
        ysr += 1

    if data["SecurityCheck"]["UACLevel"] == "Disabled":
        ysr += 3
    elif "0" in data["SecurityCheck"]["UACLevel"] or "1" in data["SecurityCheck"]["UACLevel"]:
        ysr += 1.5


    #UnHardware
    if data["SoftwareCheck"]["NoNoList"] != "False":
        ysr  += (data["SoftwareCheck"]["NoNoList"].count("|")+1)
    if data["SoftwareCheck"]["IffyList"] != "False":
        ysr  += (data["SoftwareCheck"]["IffyList"].count("|")+1)*.25


    #UnSoftware
    ysr += (data["HardwareCheck"]["BadDrives"].count("Warning"))*0.25
    ysr += (data["HardwareCheck"]["BadDrives"].count("Bad"))*1

    if "Mismatched" in data["HardwareCheck"]["MixedMemory"]: 
        ysr += 1
#    return(int((ysr+1)*ysr*0.5))
    return(ysr)








