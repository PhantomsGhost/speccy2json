from config import *
"""
Produces the basic software checks for speccy reports.
"""

### Checks the ENTIRE speccy report for any software
### that likely causes issues with an OS
def nonoCheck(body):
    nostat = []
    lowerBody = str(body).lower()
    for nono in NO_LIST_SOFTWARE:
        if nono in lowerBody:     
            nostat.append(nono)
    if nostat:
        return("|".join(nostat))
    else:
        return("False")

### Checks the ENTIRE speccy report for any antiviruses or
### remanents not seen in the AV section of speccy
def hiddenAVCheck(body):
    hidden = []
    lowerBody = str(body).lower()
    for av in AVLIST:
        if av in lowerBody:     
            hidden.append(av)
    if hidden:
        return("|".join(hidden))
    else:
        return("False")

### Checks the ENTIRE speccy report for any software
### that make be causing an issue, but isnt 100% bad
def ehhCheck(body):
    ehhstat = []
    lowerBody = str(body).lower()
    for ehh in IFFY_SOFTWARE:
        if ehh in lowerBody:     
            ehhstat.append(ehh)
    if ehhstat:
        return("|".join(ehhstat))
    else:
        return("False")    

### Checks for any possible mention of KMSPico or Auto KMS
### May have a false positive, but in testing there were none
def checkKMS(body):
    if "kms" in str(body).lower():
        return("True")
    else:
        return("False")