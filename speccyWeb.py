from flask import Flask, request
from datetime import datetime
import random
import traceback
import genSum
from config import *
import re
import hwinfo 
import cdi

SPECCY_RE = re.compile("^[a-zA-Z0-9]*$")
app = Flask(__name__)

"""
Makes the magic of web requests happen
"""

@app.route('/')
def main():
    try:
        print(request.args)
        jsonStat = False
        
        if "hwinfo" in request.args.keys():
            return(hwinfo.parseinfo(request.args['hwinfo']))
        if "cdi" in request.args.keys():
            return(cdi.parseinfo(request.args['cdi']))
        
        speccy = request.args['speccy']
        # If this is a link or item long enough to be a speccy:
        if len(speccy) >= 35:
            if speccy[0:35] == "http://speccy.piriform.com/results/":
                pass
        # If its not long enough, is it possibly a speccy ID?
        elif re.match(SPECCY_RE,speccy):
                speccy = "http://speccy.piriform.com/results/" + speccy

        # Otherwise it goes in the trash bin
        else:
            return('{"Status":"UnknownURL"}')

        # Logic to ask for json format for the TS Bot
        if "json" in request.args.keys():
            if request.args['json'].lower() == 'true':
                speccy_result = genSum.genSum(speccy)
                if len(speccy_result) > 2400:
                    return(TOO_LONG)
                return(speccy_result)
            else:
                result = genSum.genOut(speccy)
                if "Parsed" in result:
                    return("<pre>" + result.replace("\n","<br>")+ "</pre>")
        # Otherwise ask for the human readable version.
        else:
            result = genSum.genOut(speccy)
            if "Parsed" in result:
                return("<pre>" + result.replace("\n","<br>")+ "</pre>")

        if result:
            return(result)
        else:
            return('{"Status":"LogicError"}')

    # If somthing really breaks.    
    except Exception as e:
        print(e)
        traceback.print_exc()
        return(POTATO)
        return('{"Status":"Something Broke, likely a date. Consistency very hard.","link":"'+speccy+'"}')






# Using debug mode during the development 
# of the Speccy Parser
if __name__ == "__main__":
    app.run(host="127.0.0.1", debug=True)
