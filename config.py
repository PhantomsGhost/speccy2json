NO_LIST_SOFTWARE = ["iobit", "driver easy", "driver updater", "clownfish", "voicemod", 'kmspico','voicemeter',\
                        "autokms","snappy driver","ccleaner","driver booster", "debloat", "win10script", "tron script",\
                        "snappydriver", "outbyte", "systemcare", "iolo","voicemeeter","ooshutup","rainmeter","altening",\
                        "revision-pc","turbolan","turbo lan","xfast","cfosspeed","coinminer",\
                        "miner", "coin miner","atlasos","xmrig","lunar","hack","advancedwindowsmanager","santivirus","reimage","cleanpc","autopico", "driver fix", "driverfix"]

AVLIST = ["malwarebyte","spybot","mcafee","norton","sophos","avast","panda dome","comodo","webroot","Bullguard","Emsisoft","F-Secure"]

IFFY_SOFTWARE = ["razer","netlimiter","throttlestop","vpn","wallpaper engine","wallpaper","titty","ipvanish","roblox",\
                     "nicehash","torrent","taskbar","hpomen"]


SUPPORTED_WIN_VERSIONS = [8.1, 10, 11]
SUPPORTED_WIN_EDITIONS = ["Home", "Pro", "Professional"]
SUPPORTED_WIN_10_BUILDS = ["20H1","2009","20H2", "2105", "21H1", "21H2"]
UNSUPPORTED_WIN_EDITIONS = ["Enterprise", "Insider", "Preview", "Server", "Windows 7", "Vista", "XP", "ME", "Windows7"]

TEMP_LIMIT = 70

LENGTH_LIMIT = 221675
    
POTATO = """{
	"Status": "Parsed",
	"Link": "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
	"ReportDate": "Time",
	"CurrentTime": "Time",
	"OSCheck": {
		"MajorOS": "Potato",
		"MinorOS": "Yukon Gold",
		"InstallDate": "89 Days",
		"FailedHotfix": 0,
		"LastHotfixDate": "Not yet harvested",
		"BPPC": "Did pirates have potatoes?",
		"Uptime": "999.7 * 10^5 Humming bird wing flaps",
		"OSSupported": "Its a potoato",
		"DateFormat": "Potatoes dont have dates",
		"DateTimeFormat": "Potatoes also dont have time"
	},
	"SecurityCheck": {
		"AVStatus": "Natural Potato",
		"WindowsDefenderStatus": "Can be thrown at windows",
		"FirewallState": "Good roasted",
		"UACLevel": "Edible"
	},
	"SoftwareCheck": {
		"NoNoList": "Safe to eat",
		"AVDetect": "No AV?",
		"IffyList": "Smells fine",
		"SpeccyVersion": "Up To Date",
		"Layman": "Use this potato"
	},
	"HardwareCheck": {
		"BadDrives": "",
		"FreeSpaceIssues": "",
		"KillerNIC": "Safe to Eat",
		"MixedMemory": "May have had a fly on it",
		"XMP": ""
	},
	"HardwareSummary": {
		"CPU": "Potato",
		"Motherboard": "Potato",
		"RAM": "Potato",
		"GPUs": ["Potato","Potato","Potato"],
		"Storage": ["Potato","Potato","Potato","Potato"],
		"TempChecks": []
	},
	"Yikes": 852.0
}"""

TOO_LONG = '{"Status":"The speccy report contains too many details to display in Discord. 🐟"}'
