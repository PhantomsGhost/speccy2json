# List of different headers for different metrics.
# See XKCD # 927
headings = {
    "cpufreq":["Core Clocks (avg) [MHz]"],
    "cputemp":["Core 3 [°C]","Core 2 [°C]","Core 1 [°C]","Core 0 [°C]","Core 4 [°C]","Core 5 [°C]","Core 6 [°C]","Core 7 [°C]",
               "Core 8 [°C]","Core0 (CCD1) [°C]","Core1 (CCD1) [°C]","Core2 (CCD1) [°C]","Core3 (CCD1) [°C]","Core4 (CCD1) [°C]",
               "Core5 (CCD1) [°C]","Core6 (CCD2) [°C]","Core7 (CCD2) [°C]","Core8 (CCD2) [°C]","Core9 (CCD2) [°C]","Core10 (CCD2) [°C]",
               "Core11 (CCD2) [°C]","CPU (Tctl) [°C]","CPU (Tdie) [°C]","Core0 (Die1) [°C]","Core1 (Die1) [°C]","Core2 (Die1) [°C]","Core3 (Die1) [°C]",
               "Core5 (Die1) [°C]","Core6 (Die1) [°C]","Core7 (Die1) [°C]",],
    "cpuutil":["Total CPU Usage [%]"],
    "cpuvcore":["Vcore [V]"],

    "gpufreq":["GPU Clock [MHz]"],
    "gpuutil":["GPU Core Load [%]","GPU Utilization [%]"],
    "gputemp":["GPU Temperature [°C]","GPU Thermal Diode [°C]"],

    "psu12v":["+12V [V]","+12V Voltage [V]"],
    "psu5v":["+5V [V]","+5V Voltage [V]"],
    "psu3v":["3VSB [V]","3VSB Voltage [V]","+3.3V [V]"],
}