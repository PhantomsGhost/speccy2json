import re
import requests

from contextlib import closing
import json
from bs4 import BeautifulSoup as BS
import time
import datetime 
from dateutil import tz


from config import *
from drivedata import *
from softchecks import *
from oscheck import *
from hwparse import *
import yikeScale
import layman

"""
Main processing core for parsing speccy reports
"""

def genSum(url):
    
    start = time.time()
    with closing(requests.get(url, )) as response:
        soup = BS(response.content, "lxml", from_encoding=response.encoding)
    body = soup.body
    
    if len(body.text.split("\n")) > LENGTH_LIMIT:
        return('{"Status":"This speccy may consume too many resources and was terminated."}')
    if "Piriform Speccy" not in body.text:
        return(POTATO)
        return('{"Status":"Speccy report does not exist"}')
    
    
    def getTimeFormat():
        try:
            dfmt = body.find("div", class_="blue clear", text="TimeZone").next_sibling.next_sibling.find("div", class_="datakey", text=re.compile("Date Format")).next_sibling.next_sibling.text
            tfmt = body.find("div", class_="blue clear", text="TimeZone").next_sibling.next_sibling.find("div", class_="datakey", text=re.compile("Time Format")).next_sibling.next_sibling.text
            dtfmt = f"{dfmt} {tfmt}"
            return(re.sub("[^\x00-\x7F]+","",dfmt),re.sub("[^\x00-\x7F]+","",dtfmt))
        except:
            return("Unknown","Unknown")
    def genUTCOffset():
        try:
            hr = body.find("div", class_="blue clear", text='TimeZone').next_sibling.next_sibling.find("div", class_="datavalue", text=re.compile('GMT')).text
            if hr == "GMT":
                return("+0000")
            hr = hr.replace("GMT ","")
            hr = hr.replace(" Hours","")
            if len(hr) > 5:
                hr = hr.replace(":","")
            if len(hr) == 5:
                hr = hr.replace("-","-0")
                hr = hr.replace("+","+0")
                hr = hr.replace(":","")
            
            return(hr)
        except:
            return("+0000")
        
    DATE_FORMAT, DT_FORMAT = getTimeFormat()
    TIME_ZONE = genUTCOffset()
        
    def dateParsing(timestamp,fmttype):
        try:
            if DATE_FORMAT == "Unknown":
                return(timestamp)

            if fmttype == "Date":
                tf = DATE_FORMAT
            else:
                tf = DT_FORMAT

            dtConvert = {

                "d":"%d",
                "M":"%m",
                "yyyy":"%Y",
                "yyy":"%Y",
                "yy":"%y",
                "H":"%H",
                "h":"%I",
                "mm":"%M",
                "ss":"%S",
                "tt":"%p"
            }

            if "dd" in tf:
                tf = tf.replace("dd","d")
            if "MMM" in tf:
                tf = tf.replace("MMM","%b")
            if "MM" in tf:
                tf = tf.replace("MM","M")
            if "HH" in tf:
                tf = tf.replace("HH","H")
            if "hh" in tf:
                tf = tf.replace("hh","H")

            timestamp = timestamp.replace("a. m.","am")
            timestamp = timestamp.replace("p. m.","pm")

            for fm in dtConvert.keys():
                tf = tf.replace(fm,dtConvert[fm])

            return(datetime.datetime.strptime(timestamp+f" {TIME_ZONE}",tf+" %z").astimezone(tz.UTC).isoformat().replace('+00:00',' UTC'))
        except Exception as e:
            print(e)
            print(DATE_FORMAT, DT_FORMAT)
            print(timestamp)
    
    ### Attempt to get the Windows install data
    def getInstallDate():
        try:
            return(dateParsing(body.find("div", text=re.compile(r"Installation Date")).text.split("Installation Date: ")[1],"DT"))
        except Exception as e:
            print(e)
            return("Unknown")
    
    ### Attempt to get the date of the last hotfix
    def getLastHotFix(body):
        try:
            installedUpdates = body.find("div", text='Installed').next_sibling.next_sibling.next_sibling.next_element.find_all("div", class_="blue clear", text=re.compile("KB"))#.next_sibling.next_sibling.next_element.next_element.next_element.next_element.next_element.next_element.next_element.text
            if installedUpdates:
                return(dateParsing(installedUpdates[0].text.split(" ")[0],"Date"))
        except Exception as e:
            print(e)
            return("Unknown")

    ### Pull the system uptime and use as report
    ### date to make sure report is recent.
    def getReportTime():
        try:
            currentTime = body.find("div", class_="blue clear", text='Current Session').next_sibling.next_sibling.next_element.next_element.text.strip().replace("Current Time:\xa0\n","")
            return(f"{dateParsing(currentTime,'DT')}")
        except:
            return("Unknown")


    def getSpeccyVersion():
        try:
            sv = body.find("a", href="http://www.piriform.com/speccy").next_sibling.strip()
            if sv == 'v1.32.774' or sv == 'v1.32.740':
                return("Up To Date")
            else:
                return("--Out of Date--")
        except:
            return("Unknown")
    hwsum = getHardwareSum(body)
    data = {
        "Status":"Parsed",
        "Link":url,
        "ReportDate":getReportTime(),
        "CurrentTime":datetime.datetime.utcnow().isoformat().split(".")[0] +" UTC",
        "OSCheck":{
            "MajorOS":getMajorOS(body),
            "MinorOS":getMinorOS(body),
            "InstallDate":getInstallDate(),
            "FailedHotfix":getFailedUpdates(body),
            "LastHotfixDate":getLastHotFix(body),
            "BPPC":checkKMS(body),
            "Uptime":getUptime(body),
            "OSSupported":validSupportOS(body),
            "DateFormat":DATE_FORMAT,
            "DateTimeFormat":DT_FORMAT,
        },
        "SecurityCheck":{
            "AVStatus":getAVStatus(body),
            "WindowsDefenderStatus":getWinDefStat(body),
            "FirewallState":getFirewallStatus(body),
            "UACLevel":getUACStatus(body),
        },
        "SoftwareCheck":{
            "NoNoList":nonoCheck(body),
            "AVDetect":hiddenAVCheck(body),
            "IffyList":ehhCheck(body),
            "SpeccyVersion":getSpeccyVersion()
        },
        "HardwareCheck":{
            "BadDrives": getDriveHealth(genSmartJson(body)),
            "FreeSpaceIssues":getFreeSpace(genSmartJson(body)),
            "KillerNIC"  : getKillerNet(body),
            "MixedMemory": getMemMismatch(body),
            "XMP": checkXMP(body)
        },
        "HardwareSummary":hwsum
    }
    data["Yikes"] = yikeScale.YSR(data)
    data["SoftwareCheck"]["Layman"] = layman.layman(data)
    return(json.dumps(data))


def genOut(url):
    data = json.loads(genSum(url))
    if data["Status"] == "Parsed":
        return(f"""```
Speccy Report Generated at  {data["ReportDate"]}
Speccy Summary Parsed   at {data["CurrentTime"]}
---------------------------------------------------

--- OS Checks ---
OS Version    : {data["OSCheck"]["MajorOS"]} {data["OSCheck"]["MinorOS"]}
OS Supported  : {data["OSCheck"]["OSSupported"]}
OS Date of Ins: {data["OSCheck"]["InstallDate"]}
Date FMT      : {data["OSCheck"]["DateFormat"]}
DateTime FMT  : {data["OSCheck"]["DateTimeFormat"]}
Failed Updates: {data["OSCheck"]["FailedHotfix"]}
Last Hotfix   : {data["OSCheck"]["LastHotfixDate"]}
Argh?         : {data["OSCheck"]["BPPC"]}
Uptime        : {data["OSCheck"]["Uptime"]}

--- Security Checks ---
Anti Virus    : {data["SecurityCheck"]["AVStatus"]}
Win Def Status: {data["SecurityCheck"]["WindowsDefenderStatus"]}
Firewall      : {data["SecurityCheck"]["FirewallState"]}
UAC Level     : {data["SecurityCheck"]["UACLevel"]}

--- Software Checks ---
No List Softwa: {data["SoftwareCheck"]["NoNoList"]}
AV Detect All : {data["SoftwareCheck"]["AVDetect"]}
PICS          : {data["SoftwareCheck"]["IffyList"]}
Speccy Version: {data["SoftwareCheck"]["SpeccyVersion"]}

--- Hardware Checks ---
Bad Drives    : {data["HardwareCheck"]["BadDrives"]}
Low Space     : {data["HardwareCheck"]["FreeSpaceIssues"]}
Killer NIC    : {data["HardwareCheck"]["KillerNIC"]}
Memory Mixed  : {data["HardwareCheck"]["MixedMemory"]}
Possible XMP  : {data["HardwareCheck"]["XMP"]}

--- Hardware Summary ---
CPU           : {data["HardwareSummary"]["CPU"]}
Motherboard   : {data["HardwareSummary"]["Motherboard"]}
Memory        : {data["HardwareSummary"]["RAM"]}
GPU           : {" | ".join(data["HardwareSummary"]["GPUs"])}
Storage       : {" | ".join(data["HardwareSummary"]["Storage"])}
Temp Alerts   : {" | ".join(data["HardwareSummary"]["TempChecks"])}

Yikes Scale   : {yikeScale.YSR(data)}
Link        : {data["Link"]}
```""")
    else:
        return(data)
