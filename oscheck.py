import re 
from config import *
from softchecks import checkKMS
"""
Produces the checks for operating system items in Speccy
"""

### Turns singular or plural based on value
### Because Ajax146#2788 was picky about
### the easy to read output.
def pleaseAjax(num):
    if num!= 1:
        return("s")
    else:
        return("")

### Check to see if the OS is supported     
def validSupportOS(body):
    majorOS = getMajorOS(body)
    minorOS = getMinorOS(body)
    if checkKMS(body) == "True":
        return("Unsupported due to piracy")

    # Quick check for anything unsupported edition wise
    for edition in UNSUPPORTED_WIN_EDITIONS:
        if edition in majorOS:
            return("Unsupported Edition")
    # Check to make sure this is Windows
    # and not "64-Bit" or "More data is available"
    if "Windows" in majorOS:
        try:
            splitStr = "Windows"
            majorOS = (majorOS[majorOS.index(splitStr) + len(splitStr):]).strip()
            # Check the two supported OS Versions. 
            # This will likely be updated for 11
            if float(majorOS.split()[0]) == 10:
                if "Unknown" in minorOS:
                    return("Please send a screenshot of `Winver`")
                else:
                    if minorOS in SUPPORTED_WIN_10_BUILDS:
                        return("Supported")
            elif float(majorOS.split()[0]) == 8.1:
                return("Supported")
            elif float(majorOS.split()[0]) == 11:
                return("Send Winver")
        # If there is something broken
        except Exception as e:
            print(e)
            return("Unknown Support")
        #Should return here for anything 11, 7, or earlier
        return("Not Supported")
    else:
        # If we can't tell this is windows
        return("Unable to Detect OS")
    

### Get the status of Windows Defender               
def getWinDefStat(body):
    try:
        return(body.find("div", text='Windows Defender').next_sibling.next_sibling.next_element.next_element.next_element.next_element.next_element.next_element.next_element.text)
    except:
            return("Unknown")
### Get the Major version of the OS
def getMajorOS(body):
    try:
        OS = re.sub("[^\x00-\x7F]+","",body.find("div", text='Operating System').next_sibling.next_sibling.text).strip()
    except:
        return("Unknown OS")
    if "Windows10" in OS:
        return(OS.replace("Windows10", "Windows 10"))
    else:
        return(OS)
    
### Attempt to get the Windows 10+ Build Version
### This will not work if the OS has not had
### and update
def getMinorOS(body):
    try:
        # Check that this is not 8.1 or 11
        # Win 11 does not yet have builds
        # The split methods is due to regions
        if "Windows 10" in getMajorOS(body):
            updates=[]
            for update in body.find_all("div",class_="blue clear", text=re.compile(r'Windows 10 ')):
                minorUpdate = update.text.lower().replace(", "," ").split("windows 10 version ")[1].split(" ")[0][0:4]
                if re.match("\d",minorUpdate) and len(minorUpdate) == 4:
                    updates.append(minorUpdate)
            return(updates[0].upper())
        else:
            return("")
    except: 
        return("Unknown Build")

### Count the number of failed Windows Updates
def getFailedUpdates(body):
    updateStatuses = []
    pendingUpdates = body.find_all("div", class_="datakey", text=re.compile("Installation Status:"))
    for update in pendingUpdates:
        updateStatuses.append(update.next_sibling.next_sibling.text)
    return(updateStatuses.count("Failed"))

    
### Attempt to get the status of what Antiviruses are
### installed on the system, splitting multiple
def getAVStatus(body):
    try:
        if "Display Name" in body.find("div", class_="blue clear", text='Antivirus').next_sibling.next_sibling.text:
            return(body.find("div", class_="blue clear", text='Antivirus').next_sibling.next_sibling.find("div", class_ = "datakey", text=re.compile("Display Name")).next_sibling.next_sibling.text)
        else:
            av_found = body.find("div", class_="blue clear", text='Antivirus').next_sibling.next_sibling.next_sibling.next_sibling.find_all("div", class_="blue clear") 
            return("|".join([av.text for av in av_found]))
    except:
        return("Unknown")

### Attempt to get the status of the system firewall
def getFirewallStatus(body):
    try:
        return(body.find("div", text=re.compile('Firewall:')).next_sibling.next_sibling.text)
    except:
        return("Unknown")

### Get UAC Level 
def getUACStatus(body):
    try:
        if body.find("div", text=re.compile('(UAC)')).next_sibling.next_sibling.text == "Enabled":
            return(body.find("div", text=re.compile('Notify level:')).next_sibling.next_sibling.text)
        else:
            return("Disabled")
    except:
        return("Unknown")

### Attempt to get system uptime
def getUptime(body):
    try:
        seconds = int(re.sub("[^\x00-\x7F]+","",body.find("div", text=re.compile('Current Uptime:')).next_sibling.next_sibling.text.split("sec")[0].replace(",", "").replace(".", "").replace(" ", "").replace("(", "-").replace(")", "").strip()))            
        if seconds < 86400:
            return(f"{float(seconds/3600):.2f} Hour{pleaseAjax(float(seconds/3600))}")
        else:
            days = int(seconds/86400)
            return(f"{days} Day{pleaseAjax(days)} and {((seconds%86400)/3600):.2f} Hour{pleaseAjax((seconds%86400)/3600)}")
    except:
        return("Unknown")
