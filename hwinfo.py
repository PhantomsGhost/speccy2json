import pandas as pd
import numpy as np
from datetime import datetime
import os
import time, random,re
from hwconfig import headings
from urllib.parse import quote
import json




def isValidURL(url):
    # https://www.geeksforgeeks.org/check-if-an-url-is-valid-or-not-using-regular-expression/
    # Regex to check valid URL
    regex = ("((http|https)://)(www.)?" +
             "[a-zA-Z0-9@:%._\\+~#?&//=]" +
             "{2,256}\\.[a-z]" +
             "{2,6}\\b([-a-zA-Z0-9@:%" +
             "._\\+~#?&//=]*)")
     
    # Compile the ReGex
    p = re.compile(regex)
 
    # If the string is empty
    # return false
    if (url == None):
        return False
 
    # Return if the string
    # matched the ReGex
    
    if(re.search(p, url)) and url[len(url)-3:len(url)].lower() == "csv"\
        and "https://cdn.discordapp.com/attachments/" in url and " " not in url and ";" not in url:
        return True
    else:
        return False


def parseinfo(url):
    if isValidURL(url) == False:
        return("Invalid")
    new_url = quote(url).replace("https%3A//","http://")
    print(new_url)
    fname = f"/tmp/{time.time()}{random.randint(1,100)}.csv"
    os.system(f"wget {new_url} -O {fname}")
    df = pd.read_csv(fname, encoding = "ISO-8859-1",error_bad_lines=False)
    df.drop(df.tail(2).index,inplace=True)
    os.system(f"rm -f {fname}")
    #print(f"File Downloaded to {fname}")
    def validate(m,mn):
        clear = "✅"
        bad = "❌"
        caution = "🚨"
        if type(m) == str:
            if "m" in m:
                return("Unable to Detect")
        if mn == "poll":
            if m < 300:
                return(clear)
        elif mn == "elapse":
            if m > 9:
                return(clear)
        elif mn == "p12":
            if float(m.split("/")[0]) > (12*.95) and float(m.split("/")[2]) <= (12*1.05):
                return(clear)
        elif mn == "p5":
            if float(m.split("/")[0]) > (5*.95) and float(m.split("/")[2]) <= (5*1.05):
                return(clear)
        elif mn == "p3":
            if float(m.split("/")[0]) > (3.3*.95) and float(m.split("/")[2]) <= (3.3*1.05):
                return(clear)

        elif mn == "cput":
            if float(m.split("/")[2]) < 80:
                return(clear)
            elif float(m.split("/")[2]) < 90:
                return(caution)

        elif mn == "gput":
            if float(m.split("/")[2]) < 80:
                return(clear)
            elif float(m.split("/")[2]) < 90:
                return(caution)

        elif mn == "cpuu":
            if float(m.split("/")[1]) > 80:
                return(clear)

        elif mn == "gpuu":
            if float(m.split("/")[1]) > 80:
                return(clear)
        
        return(bad)

    # Get the polling rate of the HWINFO log
    def gettimerate():
        _raw = []
        times = df["Time"].iloc[1:100]
        for t in times:
            _raw.append(int(t.split(".")[1]))
        _deltas = []
        for t in range(1,len(times)):
            if _raw[t] > _raw[t-1]:
                _deltas.append(abs(_raw[t]-_raw[t-1]))
            else:
                _deltas.append((abs(_raw[t]+1000)-_raw[t-1]))
        return(round(np.mean(_deltas)))

    # Get duration of HWINFO Log
    def getelapsed():
        start = datetime.strptime(df["Time"].iloc[1].split(".")[0], "%H:%M:%S")
        end = datetime.strptime(df["Time"].iloc[-1].split(".")[0], "%H:%M:%S")
        return(round((end-start).total_seconds()/60,2))

    # Get PSU Rail Statistics
    def psustat(rail):
        raildata = []
        for col in df.columns:
            if col in headings[rail]:
                for i in df[col]:
                    raildata.append(float(i))
        if len(raildata) == 0:
            return("m/m/m")
        return(f"{round(min(raildata),2)}/{round(np.mean(raildata),2)}/{round(max(raildata),2)}")

    def cpustat(mn):
        """cpufreq":["Core Clocks (avg) [MHz]"],
        "cputemp":["Core 3 [°C]"],
        "cpuutil":["Total CPU Usage [%]"],
        "cpuvcore":["Vcore [V]"]"""
        cpudata = []
        for col in df.columns:
            if col in headings[mn]:
                print("cpustat",col)
                for i in df[col]:
                    cpudata.append(float(i))
        if len(cpudata) == 0:
            return("m/m/m")
        return(f"{round(min(cpudata),2)}/{round(np.mean(cpudata),2)}/{round(max(cpudata),2)}")


    def gpustat(mn):
        """gpufreq":["GPU Clock [MHz]"],
        "gpuutil":["GPU Core Load [%]"],
        "gputemp":["GPU Temperature [°C]"]"""
        gpudata = []
        for col in df.columns:
            if col in headings[mn]:
                for i in df[col]:
                    gpudata.append(float(i))
        if len(gpudata) == 0:
            return("m/m/m")
        return(f"{round(min(gpudata),2)}/{round(np.mean(gpudata),2)}/{round(max(gpudata),2)}")

    def ToC():
        TEMPMAX = 75
        ToCs = {}
        # Look for any temperatures above a threshold
        try:
            for col in df.columns:
                if "°C" in col:
                    dat = [float(i) for i in df[col]]
                    if max(dat) > TEMPMAX:
                        
                        ToCs[col] = f"{round(min(dat),2)}/{round(np.mean(dat),2)}/{round(max(dat),2)}"
        except:
            print(col)
        return(ToCs)






    data = {
        "pollrate":gettimerate(),
        "elapsed" :getelapsed(),
        "psu12":psustat("psu12v"),
        "psu5":psustat("psu5v"),
        "psu3":psustat("psu3v"),
        "cpufreq":cpustat("cpufreq"),
        "cputemp":cpustat("cputemp"),
        "cpuutil":cpustat("cpuutil"),
        "cpuvcore":cpustat("cpuvcore"),
        "gpufreq":gpustat("gpufreq"),
        "gpuutil":gpustat("gpuutil"),
        "gputemp":gpustat("gputemp"),
        "ToC":ToC()

    }
    ToCStr = ""
    for key in data["ToC"]:
        ToCStr += f'{key} - {data["ToC"][key]}\n'

    return(json.dumps(data))
    f"""**HWiNFO Summary:**
```
Poll Rate : {data["pollrate"]} ms {validate(data["pollrate"],"poll")}
Elapsed   : {data["elapsed"]} minutes {validate(data["elapsed"],"elapse")}
Prime95   : {validate(data["cpuutil"],"cpuu")}
Furmark   : {validate(data["gpuutil"],"gpuu")}

PSU Data-----------
12 V Rail : {data["psu12"]} Volts {validate(data["psu12"],"p12")}
5 V Rail  : {data["psu5"]} Volts {validate(data["psu5"],"p5")}
3.3 V Rail: {data["psu3"]} Volts {validate(data["psu3"],"p3")}

CPU Data ============
Frequency : {data["cpufreq"]} Mhz 
Temperatur: {data["cputemp"]} *C {validate(data["cputemp"],"cput")}
Utilizatio: {data["cpuutil"]} % 
V Core Vol: {data["cpuvcore"]} Volts 

GPU Data ++++++++++++
Frequency : {data["gpufreq"]} Mhz 
Utilizatio: {data["gpuutil"]} % 
Temperatur: {data["gputemp"]} *C {validate(data["gputemp"],"gput")}

Temps of Concern:-------------
{ToCStr}
```"""
